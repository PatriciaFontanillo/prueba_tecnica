<?php

$host = "localhost";
$user = "root";
$password = "root";
$database = "netberry";

// Connect to MySQL Database
$con = new mysqli($host, $user, $password, $database);

// Check connection
if ($con->connect_error) {
    var_dump($con->connect_error);
    die("Connection failed: " . $con->connect_error);
}

?>