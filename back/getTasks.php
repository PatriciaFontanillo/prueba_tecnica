<?php
    // include Database connection file 
    include("dbConnection.php");

    // Design initial table header 
    $data = '<table class="table table-bordered table-striped">
        <tr>
            <th>Tarea</th>
            <th>Categoría</th>
            <th>Acciones</th>
        </tr>';

    $query = "SELECT t.id ,t.text,  GROUP_CONCAT(c.name SEPARATOR '|') AS categories FROM tasks AS t
              LEFT JOIN tasks_categories AS tc ON t.id = tc.`task_id`
              LEFT JOIN categories AS c ON c.id = tc.`category_id`
              GROUP BY t.id";

    if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con));
    }

    // if query results contains rows then featch
    if(mysqli_num_rows($result) > 0)
    {
        $number = 1;
        while($row = mysqli_fetch_assoc($result))
        {
            $categories = explode("|", $row['categories']);
            $data .= '<tr id="task_'.$row['id'].'" > <td>'.$row['text'].'</td>';
            $data .=   '<td>';

            foreach($categories  as $category){
                $text = empty($category) ? '<span>sin categoría</span>' : '<span>' . $category . '</span>';
                $data .= $text ;
            }

            $data .= '</td>';
            $data .= '<td>
                    <button onclick="deleteTask(event,'.$row['id'].')">
                        <img src="/images/delete.svg" alt="Delete task" width="20" height="auto">
                    </button>
                </td>
            </tr>';
            $number++;
        }
    }
    else
    {
        // records not found 
        $data .= '<tr id="not_records" ><td colspan="6">No hay registros!</td></tr>';
    }

    $data .= '</table>';

    echo $data;
?>