// READ tasks
function getTasks() {

    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                let container = document.getElementsByTagName('tbody')[0];
                container.innerHTML = this.responseText;
            }
    };  
    req.open("GET","/back/getTasks.php");
    req.send();

}

// ADD new tasks
function addTask(event){
    event.preventDefault();
    let form = document.getElementById('task_search');
    let data = new FormData(form);
    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {

            if(this.responseText == 'ERROR: TASK CONTENT'){
                alert('Debes completar el nombre de la tarea');
            }else{
                let container = document.getElementsByTagName('tbody')[0];
                container.innerHTML = container.innerHTML + (this.responseText);
                if(container.children.length == 3){
                    let not_records = document.getElementById('not_records');
                    not_records.parentNode.removeChild(not_records);
                }
            }
        }
    };  
    req.open("POST","/back/addTask.php");
    req.send(data);
}

// DELETE tasks
function deleteTask(event,id){

    event.preventDefault();
    let data = new FormData();
    data.append('id', id);
    let req = new XMLHttpRequest();
    req.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {

            if(this.responseText == 'DELETE: SUCCESS'){
                let col_task = document.getElementById('task_' + id);
                col_task.parentNode.removeChild(col_task);
            }
            let container = document.getElementsByTagName('tbody')[0];

            if(container.children.length == 1){
                container.innerHTML = container.innerHTML + '<tr id="not_records"><td colspan="6">No hay registros!</td></tr>';
            }
            
        }
    };  
    req.open("POST","/back/deleteTask.php");
    req.send(data);
}


function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

docReady(function() {
    getTasks();
});