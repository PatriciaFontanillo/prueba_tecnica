SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS tasks (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  text text,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS categories (
    id INT unsigned NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS tasks_categories (
    id INT unsigned NOT NULL AUTO_INCREMENT,
    task_id INT unsigned NOT NULL,
    category_id INT unsigned NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (task_id)
    REFERENCES tasks(id)
    ON DELETE CASCADE
);

INSERT INTO categories (id, name) VALUES (1, 'php') ,(2, 'javascript'),(3, 'css') ;