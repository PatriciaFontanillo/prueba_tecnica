<?php

if(isset($_POST['task'])){

    if(empty($_POST['task'])){
        echo 'ERROR: TASK CONTENT';
        die();
    }

    // include Database connection file
    include("dbConnection.php");
    $text_task = $_POST['task'];

    // Insert new task
    $query = "INSERT INTO tasks(text) VALUES('$text_task')";
    if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con)); 
    }
    $task_id = $con->insert_id;

    // Get names of categories
    $query = "SELECT c.name FROM categories AS c";
    if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con)); 
    }

    $data .= '<tr id="task_'.$task_id.'" > <td>'.$text_task.'</td>';
    $data .=   '<td>';

    if(mysqli_num_rows($result) > 0)
    {
        $number = 1;
        while($row = mysqli_fetch_assoc($result))
        {
            if(isset( $_POST[$row['name']])){
                // Insert categories on aux table
                savePivotTable($task_id,intval($_POST[$row['name']]),$con);
                $tags .= '<span>' . $row['name'] . '</span>';
            }
            $number++;
        }
    }

    $tags  = empty( $tags ) ? '<span>sin categoría</span>' :  $tags;
    $data .= $tags ;
    $data .= '</td>';
    $data .= '<td>
                <button onclick="deleteTask(event,'.$task_id.')">
                    <img src="/images/delete.svg" alt="Delete task" width="20" height="auto">
                </button>
             </td>
            </tr>';

    echo $data;
    }

/**
 * Save aux table (tasks_categories)
 *
 * @param $task_id  ID of task
 * @param $category_id ID of category
 * @param $con Connection database
 */ 
function savePivotTable($task_id,$category_id,$con){
    $query = "INSERT INTO tasks_categories(task_id,category_id) VALUES('$task_id', $category_id)";
    if (!$result = mysqli_query($con, $query)) {  
        exit(mysqli_error($con)); 
    }
}

?>